import java.util.Stack;

public class ShuntingYard_V02 {
	
	static String postfix;
	static Stack<Operator> operatorsStack;
	
	public static void printState() {
		printStack();
		printPostfix();
	}
	
	public static void printStack() {
		System.out.println("Stack State : " + operatorsStack);
	}
	
	public static void printPostfix() {
		System.out.println("Postfix State : " + postfix);
		System.out.println();
	}	
	
	enum Operator{
		LEFTBRACKET("(", true), // the higher the number, the higher the presidence
		RIGHTBRACKET(")", true),
		PLUS(2, "+", "left", true),
		MINUS(2, "-", "left", true),
		DIVIDE(3, "/", "left", true),
		MULTIPLY(3, "*", "left", true),		
		POWER(4, "^", "right", true),
		EQUALS("=", true),
		OPERAND(111, false);
		
		int presidence;
		String symbol;
		boolean isOperator;
		String associativity;
		
		Operator() {
		}
		
		Operator(String symbol, boolean isOperator) {
			this.symbol = symbol;
			this.isOperator = isOperator;
		}		
		
		Operator(int pres, boolean isOperator) {
			this.presidence = pres;
			this.isOperator = isOperator;
		}
		
		Operator(int pres, String symbol, boolean isOperator) {
			this.presidence = pres;
			this.symbol = symbol;
		}	
		
		Operator(int pres, String symbol, String association, boolean isOperator) {
			this.presidence = pres;
			this.symbol = symbol;
			this.associativity = association;
		}			
		
		public int getPresidence() {
			return presidence;
		}

		public String getSymbol() {
			return symbol;
		}

		public boolean isOperator() {
			return this.isOperator;
		}

		public String getAssociation() {
			return associativity;
		}		
	}
	
	public static Operator checkToken(char aToken) { // checks the current token, its an operator - presidence value is returned, if it is an operand - it is added to the stack		
		switch(aToken) {
			case '(' : {
				System.out.println("Current Token is leftBracket - Presidence : "+Operator.LEFTBRACKET.getPresidence());
				return Operator.LEFTBRACKET;
			}
			case ')' : {
				System.out.println("Current Token is rightBracket - Presidence : "+Operator.RIGHTBRACKET.getPresidence());
				return Operator.RIGHTBRACKET;
			}
			case '+' : {
				System.out.println("Current Token is plus - Presidence : "+Operator.PLUS.getPresidence());
				return Operator.PLUS;
			}
			case '-' : {
				System.out.println("Current Token is minus - Presidence : "+Operator.MINUS.getPresidence());
				return Operator.MINUS;
			}
			case '/' : {
				System.out.println("Current Token is divide - Presidence : "+Operator.DIVIDE.getPresidence());
				return Operator.DIVIDE;
			}
			case '^' : {	
				System.out.println("Current Token is power - Presidence : "+Operator.POWER.getPresidence());
				return Operator.POWER;
			}
			case '*' : {
				System.out.println("Current Token is : multiply - Presidence : "+Operator.MULTIPLY.getPresidence());
				return Operator.MULTIPLY;
			}
			case '=' : {
				System.out.println("Current Token is : equals - Presidence : "+Operator.EQUALS.getPresidence());
				return Operator.EQUALS;
			}
			default : {
				System.out.println("Operand  Detected! : " + aToken );
				return Operator.OPERAND;
			}
		}
	}
	
	public static String getReversePolishNotation(char[] infix) {
		operatorsStack = new Stack<Operator>();
		postfix = ""; // NB: Change this to either a Queue or an array
		
		for(int i=0; i<infix.length; i++) { // while there are tokens to be read
			Operator currentToken = checkToken(infix[i]); //read the next token
			myLabel : // used to break the while loop when we find 2 instances of right associated operators
			if(currentToken.getPresidence() != 111) { // if the token is an operator
				if(operatorsStack.isEmpty()) { // if the operator stack is empty, push the operator to the stack 
					operatorsStack.push(currentToken); printState();
				} else if(currentToken.getSymbol().equals("(")) { // if the token is a left bracket (i.e. "("), then:
					operatorsStack.push(currentToken); printState(); // push it onto the operator stack.	
				} else if(!operatorsStack.isEmpty() && currentToken.getSymbol().equals(")")) { // if the token is a right bracket (i.e. ")"), then:	
					while(!operatorsStack.isEmpty() && !operatorsStack.peek().getSymbol().equals("(")) { // while the operator at the top of the stack is not a left bracket:
						postfix = postfix + operatorsStack.pop().getSymbol().toString(); printState(); // pop operators from the stack onto the output queue.
					}
					if(!operatorsStack.isEmpty()) 
						operatorsStack.pop(); printState(); // pop the left bracket from the stack.
				} else if(!operatorsStack.isEmpty()) {
					if(currentToken.getSymbol().equals("(") && operatorsStack.peek().getSymbol().equals("(") 
						|| (!currentToken.getSymbol().equals("("))){
						
						while(!operatorsStack.isEmpty() && !currentToken.getSymbol().equals("(") && !currentToken.getSymbol().equals(")") && (operatorsStack.peek().getPresidence() >= currentToken.getPresidence())) {
							if((operatorsStack.peek().getPresidence() == currentToken.getPresidence()) && (operatorsStack.peek().getAssociation().equals("right") && currentToken.getAssociation().equals("right") )) {
								System.out.println("*************************");
								System.out.println("Two instances of Right Associated Operators Detected");
								System.out.println("*************************");
								operatorsStack.push(currentToken);	
								break myLabel;
							}
							else if(operatorsStack.peek().getPresidence() == currentToken.getPresidence()) { // if both operators are of equal presidence
								postfix = postfix + operatorsStack.pop().getSymbol().toString(); printState();														
							} else if(operatorsStack.peek().getPresidence() > currentToken.getPresidence()) {
								postfix = postfix + operatorsStack.pop().getSymbol().toString(); printState();
							}
						}
						operatorsStack.push(currentToken); printState();
					} else if(operatorsStack.peek().getPresidence() < currentToken.getPresidence()) {
						operatorsStack.push(currentToken); printState();
					}
				}
			} else if(currentToken.getPresidence() == 111){ // if the token is not an operator
				postfix = postfix + infix[i]; printState();
			}			
		} // end of for-loop
		
		// if there are no more tokens to read:
		while(!operatorsStack.isEmpty()) { // while there are still operator tokens on the stack:
			postfix = postfix + operatorsStack.pop().getSymbol().toString(); // pop the operator onto the output queue 
			printState();
		}		
		return postfix;
	}
	
	public static void main(String[] args) {
		String input;
		//String input = "2 + (3 * (8 - 4)) ";
		//String input = "1 + 2 * 3";
		//String input = "3 + 4 * 2 / ( 1 - 5 ) ^ 2 ^ 3";
		//String input = "A * (B + C)";
		//String input = "A - B + C "; // 
		//String input = "A * B ^ C + D "; // 
		//String input = "A * (B + C * D) + E";		
		//input = "A + B";
		//input = "A ^ 2 + 2 * A * B + B ^ 2";
		//input = "((1+2)/3)^4";
		//input = "(1+2)*(3/4)^(5+6)";
		//input = "3 + 4 * 2 / ( 1 − 5 ) ^ 2 ^ 3";
		//input = "f=a*c^k/p-q*g^(h-b)";
		//input = "a+b*(c-d)";		
		//input = "(A+B/C*(D+E)-F)";
		input = "3 + 4 * 2 / (1 - 5) ^ 2 ^ 3";
		//input = "(1+2)*(3/4)^(5+6)";		
		//input = "A^2+2*A*B+B^2";
		
		input = input.replaceAll(" ", "");
		char[] infix = input.toCharArray();		
		System.out.println(infix);

		System.out.println(getReversePolishNotation(infix));
	}
}
